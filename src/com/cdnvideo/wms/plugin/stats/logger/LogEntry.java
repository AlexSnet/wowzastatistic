package com.cdnvideo.wms.plugin.stats.logger;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LogEntry {

    private List<LogEntry> subentries = null;

    private String vhost;
    private long time = System.currentTimeMillis() / 1000L;
    private String parentId;
    private String protocol;
    private String serverUrl;
    private String state;

    private String client;
    private long inBytes;
    private long outBytes;
    private long totalOutBytes;
    private long totalInBytes;
    private long duration;
    private String referrer;
    private String ip;
    private String userAgent;
    private String applicationName;
    private String instanceName;
    private String streamName;
    private String requestId;
    private LogGeoEntry geo;
    private boolean feeded = false;

    public LogEntry() {
        // What's up, man?
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public long getTotalOutBytes() {
        return totalOutBytes;
    }

    public void setTotalOutBytes(long totalOutBytes) {
        this.totalOutBytes = totalOutBytes;
    }

    public long getTotalInBytes() {
        return totalInBytes;
    }

    public void setTotalInBytes(long totalInBytes) {
        this.totalInBytes = totalInBytes;
    }

    public LogGeoEntry getGeo() {
        return geo;
    }

    public void setGeo(LogGeoEntry geo) {
        this.geo = geo;
    }

    public long getTime() {
        return time;
    }

    public String getParentId() {
        return (parentId != null) ? parentId : "";
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getServer() {
        return LogWorker.getInstance().getServername();
    }

    public String getVhost() {
        return (vhost != null) ? vhost : "";
    }

    public void setVhost(String vhost) {
        this.vhost = vhost;
    }

    public String getRequestId() {
        return (requestId != null) ? requestId : "";
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getServerUrl() {
        return (serverUrl != null) ? serverUrl : "";
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getProtocol() {
        return (protocol != null) ? protocol : "UNKNOWN";
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getState() {
        return (state != null) ? state : "";
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getInBytes() {
        return inBytes;
    }

    public void setInBytes(long inBytes) {
        this.inBytes = inBytes;
    }

    public long getOutBytes() {
        return outBytes;
    }

    public void setOutBytes(long outBytes) {
        this.outBytes = outBytes;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getReferrer() {
        return (referrer != null) ? referrer : "";
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getIp() {
        return (ip != null) ? ip : "";
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return (userAgent != null) ? userAgent : "";
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getApplicationName() {
        return (applicationName != null) ? applicationName : "";
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getInstanceName() {
        return (instanceName != null) ? instanceName : "";
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getStreamName() {
        return (streamName != null) ? streamName : "";
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public void addSubentry(LogEntry entry) {
        if (subentries == null)
            subentries = new ArrayList<LogEntry>();

        // Set up some params from parent
        entry.setParentId(getRequestId());
        entry.setApplicationName(getApplicationName());
        entry.setInstanceName(getInstanceName());
        entry.setVhost(getVhost());

        entry.setReferrer(getReferrer());
        entry.setIp(getIp());
        entry.setUserAgent(getUserAgent());

        entry.setProtocol(getProtocol());

        subentries.add(entry);
    }

    public boolean hasSubentries() {
        return (subentries != null) && (subentries.size() > 0);
    }

    public List<LogEntry> getSubentries() {
        return subentries;
    }

    /**
     * Build log message in strict order
     *
     * @return log message
     */
    @Override
    public String toString() {
        StringBuilder logBuilder = new StringBuilder();
        // Hardcoded log format
        logBuilder.append(Long.toString(time));               // 0    timestamp
        logBuilder.append("\t");
        logBuilder.append(getRequestId());                    // 1    request id
        logBuilder.append("\t");
        logBuilder.append(getParentId());                     // 2    parent id
        logBuilder.append("\t");
        logBuilder.append(getClient());                       // 3    client name
        logBuilder.append("\t");
        logBuilder.append(getState());                        // 4    state
        logBuilder.append("\t");
        logBuilder.append(getVhost());                        // 5    vhost
        logBuilder.append("\t");
        logBuilder.append(getServer());                       // 6    server
        logBuilder.append("\t");
        logBuilder.append(getIp());                           // 7    ip
        logBuilder.append("\t");
        logBuilder.append(getProtocol());                     // 8    protocol
        logBuilder.append("\t");
        logBuilder.append(getServerUrl());                    // 9    url
        logBuilder.append("\t");
        logBuilder.append(getApplicationName());              // 10   application
        logBuilder.append("\t");
        logBuilder.append(getInstanceName());                 // 11   instance
        logBuilder.append("\t");
        logBuilder.append(getStreamName());                   // 12   stream
        logBuilder.append("\t");
        logBuilder.append(getUserAgent());                    // 13   user-agent
        logBuilder.append("\t");
        logBuilder.append(getReferrer());                     // 14   referrer
        logBuilder.append("\t");
        logBuilder.append(Long.toString(getInBytes()));       // 15   delta in bytes
        logBuilder.append("\t");
        logBuilder.append(Long.toString(getOutBytes()));      // 16   delta out bytes
        logBuilder.append("\t");
        logBuilder.append(Long.toString(getTotalInBytes()));  // 17   total in bytes
        logBuilder.append("\t");
        logBuilder.append(Long.toString(getTotalOutBytes())); // 18   total out bytes
        logBuilder.append("\t");
        logBuilder.append(Long.toString(getDuration()));      // 19   delta print duration

        if (geo != null) {                                    // 20   geo data
            // Put GeoIP data

            // LogGeoEntry geo = new LogGeoEntry( getIp() );
            logBuilder.append("\t");
            logBuilder.append(geo.toString());

        } else {
            logBuilder.append("\tUNKNOWN");
        }

        logBuilder.append("\tmedia");                         // 21   Indicates media type (media, static, etc...)

        return logBuilder.toString();
    }

    public void feed() {
        // Feed to log OR logging server
        LogWorker.getInstance().logger.info(this.toString());
        feeded = true;


        // Print subentries if exists
        if (hasSubentries()) {
            Iterator<LogEntry> it = getSubentries().iterator();
            while (it.hasNext()) {
                it.next().feed();
            }
        }
    }

    public boolean isFeeded() {
        return feeded;
    }
}