package com.cdnvideo.wms.plugin.stats.logger;

import com.cdnvideo.wms.plugin.stats.StatsModuleMain;
import com.cdnvideo.wms.plugin.stats.connection.ConnectionFactory;
import com.cdnvideo.wms.plugin.stats.connection.IConnectionInfo;
import com.cdnvideo.wms.plugin.stats.geoip.LookupService;
import com.wowza.wms.logging.*;
import com.wowza.wms.server.*;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

public class LogWorker extends java.lang.Thread {
    private static LogWorker instance = null;

    private boolean working;
    protected WMSLogger logger = null;

    private long updateInterval;

    private String servername;
    public String getServername() {
        return servername;
    }

    private LookupService geoip = null;
    public LookupService getGeoIP(){
        return geoip;
    }

    private static List<StatsModuleMain> statsmodules = new ArrayList<StatsModuleMain>();

    public static LogWorker getInstance() {
        if (instance == null) {
            instance = new LogWorker();
        }
        return instance;
    }

    private LogWorker() {
        super();
        working = true;

        // Set some options
        this.setName("WowzaStatisticsWorker");
        this.setDaemon(true);

        // Getting and registering logger
        logger = WMSLoggerFactory.getInstance().getLoggerObj("StatisticsLogger");

        // Print some meta to log
        logger.info("# Wowza statistics module configured and rant.");
        logger.info("# Start time: " + Calendar.getInstance().getTime().toString() );

        // Set a couple of server-defined options
        servername = Server.getInstance().getProperties().getPropertyStr("ServerName");
        if (servername == null || servername.equals(""))
            servername = getHostname();
        if (servername == null || servername.equals("")) {
            logger.error("# Server name is UNKNOWN.");
            servername = "UNKNOWN";
        } else
            logger.info("# Server name: " + servername);

        updateInterval = Server.getInstance().getProperties().getPropertyLong("LogFeedInterval", 10000);
        logger.info("# Update interval: " + Long.toString(updateInterval) + " msec");

        // GeoIP
        String geoipdb = Server.getInstance().getProperties().getPropertyStr("GeoIPDBPath");
        if (geoipdb != null && geoipdb.equals("")) {
            logger.info("# GeoIP is ON. (Database: " + geoipdb + ")");
            try {
                geoip = new LookupService(geoipdb, LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);
            } catch (IOException e) {
                geoip = null;
                logger.error("# Can not load GEOIP database.");
            }
        } else
            logger.info("# GeoIP functionality is turned off.");

        // Run in server thread pool
        // As I guess it is the alias to start(), but working in a thread pool.
        Server.getInstance().getThreadPool().execute(this);

    }

//    @SuppressWarnings({"unchecked", "InfiniteLoopStatement"})
    @Override
    public void run() {
        logger.info("# LogWorker Thread run loop started");

        do {
            logger.info("# Module instances loaded: " + statsmodules.size());

            // Iterate over all connections
            Iterator it = ConnectionFactory.clients.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                IConnectionInfo conn = (IConnectionInfo) pair.getValue();
                conn.getLogEntry().feed();
            }


            try {
                Thread.sleep(updateInterval);
            } catch (InterruptedException e) {
                logger.error("# LogWorker interrupted while sleeping");
                e.printStackTrace();
            }
        } while (working);
    }

    public void enjoy(StatsModuleMain statsmain) {
        logger.info("# Adding stats module: " + statsmain.toString());
        statsmodules.add(statsmain);
    }

    public void goodbye(StatsModuleMain statsmain) {
        logger.info("# Removing stats module: " + statsmain.toString());
        statsmodules.remove(statsmain);
    }

    private String getHostname() {
        // try InetAddress.LocalHost first;
        // NOTE -- InetAddress.getLocalHost().getHostName() will not work in certain environments.
        try {
            String result = InetAddress.getLocalHost().getHostName();
            if (StringUtils.isNotEmpty(result))
                return result;
        } catch (UnknownHostException e) {
            // failed;  try alternate means.
        }

        // try environment properties.
        //
        String host = System.getenv("COMPUTERNAME");
        if (host != null)
            return host;
        host = System.getenv("HOSTNAME");
        if (host != null)
            return host;

        // undetermined.
        return null;
    }
}
