package com.cdnvideo.wms.plugin.stats.logger;

public enum LogState {
    START,
    FLUSH,
    SEEK,
    PAUSE,
    STOP,
    FINISH
}
