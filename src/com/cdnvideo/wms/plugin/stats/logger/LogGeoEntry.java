package com.cdnvideo.wms.plugin.stats.logger;

import com.cdnvideo.wms.plugin.stats.geoip.Country;
import com.cdnvideo.wms.plugin.stats.geoip.Location;
import com.cdnvideo.wms.plugin.stats.geoip.Region;

public class LogGeoEntry {
    private Location location;

    public LogGeoEntry(String ip) {
        location = LogWorker.getInstance().getGeoIP().getLocation(ip);
    }

    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append( location.city );
        ret.append("/");
        ret.append( location.region );
        ret.append("/");
        ret.append( location.countryCode );
        return ret.toString();
    }
}
