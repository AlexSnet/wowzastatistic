package com.cdnvideo.wms.plugin.stats.connection;

import com.wowza.wms.rtp.model.RTPSession;
import com.wowza.wms.rtp.model.RTPStream;
import com.wowza.wms.stream.IMediaStream;

public class RTPConnectionInfo extends AbstractConnectionInfo {
    private RTPSession session;

    public RTPConnectionInfo(RTPSession rtpSession) {
        session = rtpSession;
//        session.addActionListener();
    }

    @Override
    public String getWowzaSession() {
        return session.getSessionId() + "RTPSession";
    }

    @Override
    public String getUserAgent() {
        return session.getUserAgent();
    }

    @Override
    public String getIpAddress() {
        return session.getIp();
    }

    @Override
    public String getReferrer() {
        return session.getReferrer();
    }

    @Override
    public String getProtocol() {
        return "RTSP";
    }

    @Override
    public String getVHostName() {
        return session.getVHost().getName();
    }

    @Override
    public String getApplicationName() {
        return session.getAppInstance().getApplication().getName();
    }

    @Override
    public String getApplicationInstanceName() {
        return session.getAppInstance().getName();
    }

    @Override
    public String getStreamName() {
//        RTPStream stream = session.getRTSPStream();
//        IMediaStream mStream = stream.getStream();
//        return mStream.getName();
//        session.getRTSPStream().getStreamName();
        return super.getStreamName();
    }

    @Override
    public long getTotalBytesIn() {
        return session.getIOPerformanceCounter().getMessagesInBytes();
    }

    @Override
    public long getTotalBytesOut() {
        return session.getIOPerformanceCounter().getMessagesOutBytes();
    }

    @Override
    public String getServerUrl() {
        return session.getUri();
    }

    @Override
    public String getClient() {
        return session.getAppInstance().getApplication().getProperties().getPropertyStr("ClientName");
    }
}
