package com.cdnvideo.wms.plugin.stats.connection;

import com.cdnvideo.wms.plugin.stats.logger.LogEntry;
import com.cdnvideo.wms.plugin.stats.logger.LogState;
import com.wowza.wms.stream.IMediaStream;

public interface IConnectionInfo {
    String getWowzaSession();

    String getUserAgent();

    String getIpAddress();

    String getReferrer();

    String getProtocol();

    String getClient();

    String getVHostName();

    String getApplicationName();

    String getApplicationInstanceName();

    String getStreamName();

    String getServerUrl();

    boolean canHandleSeparatedStreams();

    void onStreamCreate(IMediaStream stream);

    void onStreamDestroy(IMediaStream stream);

    LogEntry getLogEntry();

    long getBytesIn();

    long getBytesOut();

    long getTotalBytesIn();

    long getTotalBytesOut();


    LogState getState();
    void setState(LogState state);
}
