package com.cdnvideo.wms.plugin.stats.connection;

import com.cdnvideo.wms.plugin.stats.logger.LogState;
import com.wowza.wms.client.IClient;
import com.wowza.wms.stream.IMediaStream;

public class RTMPConnectionInfo extends AbstractConnectionInfo {
    private IClient client;

    public RTMPConnectionInfo(IClient client) {
        super();
        this.client = client;
    }

    @Override
    public String getWowzaSession() {
        return client.getClientId() + "RTMPSession";
    }

    @Override
    public boolean canHandleSeparatedStreams() {
        return true;
    }

    @Override
    public String getUserAgent() {
        return client.getFlashVer();
    }

    @Override
    public String getIpAddress() {
        return client.getIp();
    }

    @Override
    public String getReferrer() {
        return client.getReferrer();
    }

    @Override
    public String getProtocol() {
        return (client.getProtocol() == 1) ? "RTMP" : ((client.getProtocol() == 3) ? "RTMPT" : "UNKNOWN");
    }

    @Override
    public String getVHostName() {
        return client.getVHost().getName();
    }

    @Override
    public String getApplicationName() {
        return client.getApplication().getName();
    }

    @Override
    public String getApplicationInstanceName() {
        return client.getAppInstance().getName();
    }

    @Override
    public void onStreamCreate(IMediaStream stream) {
        IConnectionInfo conn = new RTMPStream(stream, this);
        subentries.put(stream, conn);
        conn.getLogEntry().feed();
    }

    @Override
    public void onStreamDestroy(IMediaStream stream) {
        if (subentries.contains(stream)) {
            IConnectionInfo conn = subentries.get(stream);

            subentries.remove(stream);

            conn.setState(LogState.FINISH);
            conn.getLogEntry().feed();

        }
    }

    @Override
    public long getTotalBytesOut() {
        return client.getTotalIOPerformanceCounter().getMessagesOutBytes() - client.getMediaIOPerformanceCounter().getMessagesOutBytes();
    }

    @Override
    public long getTotalBytesIn() {
        return client.getTotalIOPerformanceCounter().getMessagesInBytes() - client.getMediaIOPerformanceCounter().getMessagesInBytes();
    }

    @Override
    public String getClient() {
        return client.getApplication().getProperties().getPropertyStr("ClientName");
    }

    @Override
    public String getServerUrl() {
        return client.getUri();
    }
}
