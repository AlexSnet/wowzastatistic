package com.cdnvideo.wms.plugin.stats.connection;

import com.wowza.wms.httpstreamer.model.IHTTPStreamerSession;

public class HTTPConnectionInfo extends AbstractConnectionInfo {
    private IHTTPStreamerSession session;

    public HTTPConnectionInfo(IHTTPStreamerSession httpSession) {
        super();
        session = httpSession;
    }

    @Override
    public String getWowzaSession() {
        return session.getSessionId() + "HTTPSession";
    }

    @Override
    public String getUserAgent() {
        return session.getUserAgent();
    }

    @Override
    public String getIpAddress() {
        return session.getIpAddress();
    }

    @Override
    public String getProtocol() {
        String proto;

        switch (session.getSessionProtocol()) {

            case IHTTPStreamerSession.SESSIONPROTOCOL_CUPERTINOSTREAMING:
                proto = "CUPERTINO";
                break;
            case IHTTPStreamerSession.SESSIONPROTOCOL_SANJOSESTREAMING:
                proto = "SANJOSE";
                break;
            case IHTTPStreamerSession.SESSIONPROTOCOL_SMOOTHSTREAMING:
                proto = "SMOOTH";
                break;
            default:
                proto = "UNKNOWN";
        }

        return proto;
    }

    @Override
    public String getReferrer() {
        return session.getReferrer();
    }

    @Override
    public String getVHostName() {
        return session.getVHost().getName();
    }

    @Override
    public String getApplicationName() {
        return session.getAppInstance().getApplication().getName();
    }

    @Override
    public String getApplicationInstanceName() {
        return session.getAppInstance().getName();
    }

    @Override
    public String getStreamName() {
        return session.getStreamName();
    }

    @Override
    public String getServerUrl() {
        return session.getUri();
    }

    @Override
    public long getTotalBytesOut() {
        return session.getIOPerformanceCounter().getMessagesOutBytes();
    }

    @Override
    public long getTotalBytesIn() {
        return session.getIOPerformanceCounter().getMessagesInBytes();
    }

    @Override
    public String getClient() {
        return session.getAppInstance().getApplication().getProperties().getPropertyStr("ClientName");
    }
}
