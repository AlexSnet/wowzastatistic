package com.cdnvideo.wms.plugin.stats.connection;

import com.cdnvideo.wms.plugin.stats.logger.LogEntry;
import com.cdnvideo.wms.plugin.stats.logger.LogGeoEntry;
import com.cdnvideo.wms.plugin.stats.logger.LogState;
import com.cdnvideo.wms.plugin.stats.logger.LogWorker;
import com.wowza.wms.stream.IMediaStream;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AbstractConnectionInfo implements IConnectionInfo {
    // Subentries
    protected ConcurrentHashMap<IMediaStream, IConnectionInfo> subentries = new ConcurrentHashMap<IMediaStream, IConnectionInfo>();

    // Identifier
    private String requestIdentifier;

    // Current state
    private LogState state;

    // Last feed
    private long lastLogEntryGeneration = 0;

    // Counters
    private long lastFlushedBytesIn = 0;
    private long lastFlushedBytesOut = 0;
    // Geographic
    private LogGeoEntry geoEntry;

    public AbstractConnectionInfo() {
        if (LogWorker.getInstance().getGeoIP() != null && geoEntry == null) {
            geoEntry = new LogGeoEntry(getIpAddress());
        }

    }

    public long getLastFlushedBytesIn() {
        return lastFlushedBytesIn;
    }

    public void setLastFlushedBytesIn(long lastFlushedBytesIn) {
        this.lastFlushedBytesIn = lastFlushedBytesIn;
    }

    public long getLastFlushedBytesOut() {
        return lastFlushedBytesOut;
    }

    public void setLastFlushedBytesOut(long lastFlushedBytesOut) {
        this.lastFlushedBytesOut = lastFlushedBytesOut;
    }

    @Override
    public String getWowzaSession() {
        return null;
    }

    public String getRequestIdentifier() {
        if (this.requestIdentifier == null)
            this.requestIdentifier = UUID.randomUUID().toString();
        return this.requestIdentifier;
    }

    public LogState getState() {

        if (state == LogState.START) setState(LogState.FLUSH);
        if (state == null) setState(LogState.START);

        return state;
    }

    public void setState(LogState state) {
        this.state = state;
    }

    @Override
    public String getUserAgent() {
        return null;
    }

    @Override
    public String getIpAddress() {
        return null;
    }

    @Override
    public String getReferrer() {
        return null;
    }

    @Override
    public String getProtocol() {
        return null;
    }

    @Override
    public String getVHostName() {
        return null;
    }

    @Override
    public String getApplicationName() {
        return null;
    }

    @Override
    public String getApplicationInstanceName() {
        return null;
    }

    @Override
    public String getStreamName() {
        return null;
    }

    @Override
    public String getServerUrl() {
        return null;
    }

    @Override
    public boolean canHandleSeparatedStreams() {
        return false;
    }

    public void onStreamCreate(IMediaStream stream) {
    }

    public void onStreamDestroy(IMediaStream stream) {
    }

    // Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractConnectionInfo)) return false;

        AbstractConnectionInfo that = (AbstractConnectionInfo) o;

        if (requestIdentifier != null ? !requestIdentifier.equals(that.requestIdentifier) : that.requestIdentifier != null)
            return false;
        if (state != that.state) return false;

        return true;
    }

    // Hash code
    @Override
    public int hashCode() {
        int result = requestIdentifier != null ? requestIdentifier.hashCode() : 0;
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    @Override
    public LogEntry getLogEntry() {
        LogEntry current = new LogEntry();

        // Log entry generation delay
        if (lastLogEntryGeneration == 0)
            current.setDuration(0);
        else
            current.setDuration(current.getTime() - lastLogEntryGeneration);
        lastLogEntryGeneration = current.getTime();


        current.setRequestId(getRequestIdentifier());

        current.setApplicationName(getApplicationName());
        current.setInstanceName(getApplicationInstanceName());
        current.setVhost(getVHostName());
        current.setStreamName(getStreamName());

        current.setClient(getClient());

        current.setServerUrl(getServerUrl());

        current.setProtocol(getProtocol());
        current.setIp(getIpAddress());
        current.setReferrer(getReferrer());
        current.setState(getState().toString());

        // Traffic
        current.setInBytes(getBytesIn());
        current.setOutBytes(getBytesOut());

        current.setTotalInBytes(getTotalBytesIn());
        current.setTotalOutBytes(getTotalBytesOut());

        if (geoEntry != null) {
            current.setGeo(geoEntry);
        }

        if (this.canHandleSeparatedStreams()) {
            // Iterate over streams
            Iterator it = subentries.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                IConnectionInfo conn = (IConnectionInfo) pair.getValue();
                current.addSubentry(conn.getLogEntry());
            }
        }

        return current;
    }

    @Override
    public String getClient() {
        return null;
    }

    @Override
    public long getBytesIn() {
        long temp = getLastFlushedBytesIn();
        setLastFlushedBytesIn(getTotalBytesIn());
        return getLastFlushedBytesIn() - temp;
    }

    @Override
    public long getBytesOut() {
        long temp = getLastFlushedBytesOut();
        setLastFlushedBytesOut(getTotalBytesOut());
        return getLastFlushedBytesOut() - temp;
    }

    @Override
    public long getTotalBytesIn() {
        return 0;
    }

    @Override
    public long getTotalBytesOut() {
        return 0;
    }

}
