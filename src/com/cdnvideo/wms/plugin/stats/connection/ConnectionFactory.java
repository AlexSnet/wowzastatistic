package com.cdnvideo.wms.plugin.stats.connection;

import com.cdnvideo.wms.plugin.stats.logger.LogState;
import com.wowza.wms.client.IClient;
import com.wowza.wms.httpstreamer.model.IHTTPStreamerSession;
import com.wowza.wms.rtp.model.RTPSession;
import com.wowza.wms.stream.IMediaStream;

import java.util.concurrent.ConcurrentHashMap;

public class ConnectionFactory {
    public static ConcurrentHashMap<String, IConnectionInfo> clients = new ConcurrentHashMap<String, IConnectionInfo>();

    // Only static usage
    private ConnectionFactory() {
    }

    public static IConnectionInfo getConnectionInfo(IHTTPStreamerSession httpSession) {
        String sessionId = httpSession.getSessionId() + "HTTPSession";

        if (!clients.containsKey(sessionId))
            clients.put(sessionId, new HTTPConnectionInfo(httpSession));

        return clients.get(sessionId);
    }

    public static IConnectionInfo getConnectionInfo(RTPSession rtpSession) {
        String sessionId = rtpSession.getSessionId() + "RTPSession";

        if (!clients.containsKey(sessionId))
            clients.put(sessionId, new RTPConnectionInfo(rtpSession));

        return clients.get(sessionId);
    }

    public static IConnectionInfo getConnectionInfo(IClient client) {
        String sessionId = client.getClientId() + "RTMPSession";

        if (!clients.containsKey(sessionId))
            clients.put(sessionId, new RTMPConnectionInfo(client));

        return clients.get(sessionId);
    }

    public static void closeConnectionInfo(IConnectionInfo connection) {
        if (clients.containsKey(connection.getWowzaSession())) {

            connection.setState(LogState.FINISH);
            connection.getLogEntry().feed();

            clients.remove(connection.getWowzaSession());
        }
    }

    public static IConnectionInfo getConnectionInfoFromStream(IMediaStream stream) {
        if(stream == null)
            return null;

        if (stream.getClient() != null)
            return ConnectionFactory.getConnectionInfo(stream.getClient());

        else if (stream.getHTTPStreamerSession() != null)
            return ConnectionFactory.getConnectionInfo(stream.getHTTPStreamerSession());

        else if (stream.getRTPStream() != null)
            if (stream.getRTPStream().getSession() != null)
                return ConnectionFactory.getConnectionInfo(stream.getRTPStream().getSession());
            else
                return null;

        else
            return null;
    }
}
