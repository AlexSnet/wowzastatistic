package com.cdnvideo.wms.plugin.stats.connection;

import com.cdnvideo.wms.plugin.stats.logger.LogState;
import com.wowza.wms.stream.IMediaStream;
import com.wowza.wms.stream.IMediaStreamActionNotify;

public class RTMPStream extends AbstractConnectionInfo implements IMediaStreamActionNotify {

    private IMediaStream stream;
    private IConnectionInfo parent;

    public RTMPStream(IMediaStream s, IConnectionInfo p) {
        stream = s;
        parent = p;

        s.addClientListener(this);
    }

    @Override
    public void onPlay(IMediaStream iMediaStream, String s, double v, double v2, int i) {
        assert iMediaStream == stream;

        setState(LogState.START);
        parent.getLogEntry().feed();
    }

    @Override
    public void onPublish(IMediaStream iMediaStream, String s, boolean b, boolean b2) {

    }

    @Override
    public void onUnPublish(IMediaStream iMediaStream, String s, boolean b, boolean b2) {

    }

    @Override
    public void onPause(IMediaStream iMediaStream, boolean b, double v) {
        setState(LogState.PAUSE);
        parent.getLogEntry().feed();
    }

    @Override
    public void onSeek(IMediaStream iMediaStream, double v) {
        setState(LogState.FLUSH);
    }

    @Override
    public void onStop(IMediaStream iMediaStream) {
        assert iMediaStream == stream;

        setState(LogState.STOP);
        parent.getLogEntry().feed();
    }

    @Override
    public String getWowzaSession() {
        return parent.getWowzaSession();
    }

    @Override
    public String getUserAgent() {
        return parent.getUserAgent();
    }

    @Override
    public String getIpAddress() {
        return parent.getIpAddress();
    }

    @Override
    public String getReferrer() {
        return parent.getReferrer();
    }

    @Override
    public String getProtocol() {
        return parent.getProtocol();
    }

    @Override
    public String getVHostName() {
        return parent.getVHostName();
    }

    @Override
    public String getApplicationName() {
        return parent.getApplicationName();
    }

    @Override
    public String getApplicationInstanceName() {
        return parent.getApplicationInstanceName();
    }

    @Override
    public String getStreamName() {
        return stream.getName();
    }

    @Override
    public long getTotalBytesIn() {
        return stream.getMediaIOPerformance().getMessagesInBytes();
    }

    @Override
    public long getTotalBytesOut() {
        return stream.getMediaIOPerformance().getMessagesOutBytes();
    }

    @Override
    public String getServerUrl() {
        return super.getServerUrl();
    }

    @Override
    public String getClient() {
        return parent.getClient();
    }
}
