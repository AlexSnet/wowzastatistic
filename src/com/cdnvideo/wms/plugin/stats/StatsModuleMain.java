package com.cdnvideo.wms.plugin.stats;

import com.cdnvideo.wms.plugin.stats.connection.ConnectionFactory;
import com.cdnvideo.wms.plugin.stats.connection.IConnectionInfo;
import com.cdnvideo.wms.plugin.stats.logger.LogWorker;
import com.wowza.util.IOPerformanceCounter;
import com.wowza.wms.amf.AMFDataList;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.client.IClient;
import com.wowza.wms.httpstreamer.model.IHTTPStreamerSession;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.request.RequestFunction;
import com.wowza.wms.stream.IMediaStream;

/**
 * Installation:
 *  Put JAR file to Wowza's lib folder.
 *  Add to EACH Application.xml file strings to load module.
 *
 *  Class: com.cdnvideo.wms.plugin.stats.StatsModuleMain
 *
 *  Example:
 *
 *  <Module>
 *      <Name>WowzaStatistics</Name>
 *      <Description>Logs usage statistic in configured intervals</Description>
 *      <Class>com.cdnvideo.wms.plugin.stats.StatsModuleMain</Class>
 *  </Module>
 *
 * Configuration properties in Server.xml
 *  - LogFeedInterval
 *      Interval between feeds in milliseconds (default: 10000)
 *  - ServerName
 *      Name of the server to print
 *  - GeoIPDBPath
 *      Path to MaxMind's GeoIP Database v1, e.g.: /var/www/geodata/GeoIPCity.dat
 *      If configured than print HeoIP info in log messages
 *
 * Configuration properties in Application.xml
 *  - ClientName
 *      Name of the client
 *
 *
 * All the property names are case-sensitive and optional!
 *
 *
 * Configuration of log4j (log4j.properties) to log in separate files:
 *
 *  #### WowzaStatistics ####
 *  log4j.logger.StatisticsLogger=INFO, stdout, wowzaStatistics
 *
 *  log4j.appender.wowzaStatistics=org.apache.log4j.DailyRollingFileAppender
 *  log4j.appender.wowzaStatistics.File=/cdn/log/wowza/wowza-statistics.log
 *  log4j.appender.wowzaStatistics.layout=com.wowza.wms.logging.ECLFPatternLayout
 *  log4j.appender.wowzaStatistics.layout.Fields=x-comment
 *  log4j.appender.wowzaStatistics.layout.OutputHeader=true
 *  log4j.appender.wowzaStatistics.layout.QuoteFields=false
 *  log4j.appender.wowzaStatistics.layout.Delimeter=tab
 *  log4j.additivity.wowzaStatistics=false
 *
 *
 *  Build instructions:
 *  Just run `ant jar` to build JAR.
 *  To build jar and automatically put it in Wowza's lib folder run `ant copy`.
 *
 *  If something gets wrong (no rights, etc) try to add `sudo` at the beginning.
 *
 */
public class StatsModuleMain extends ModuleBase {
    private static LogWorker worker = LogWorker.getInstance();

    public StatsModuleMain() {
        super();
        getLogger().debug("WSM::StatsModuleMain");
    }

    // App instance listeners
    
    public void onAppStart(IApplicationInstance appInstance) {
        getLogger().debug("WSM::onAppStart");
        worker.enjoy(this);
    }

    
    public void onAppStop(IApplicationInstance appInstance) {
        getLogger().debug("WSM::onAppStop");
        worker.goodbye(this);
    }

    //This will work for Cupertino, Smooth and SanJose sessions
    public void onHTTPSessionCreate(IHTTPStreamerSession httpSession) {
        getLogger().debug("WSM::onHTTPSessionCreate");

        ConnectionFactory.getConnectionInfo(httpSession);
    }

    public void onHTTPSessionDestroy(IHTTPStreamerSession httpSession) {
        getLogger().debug("WSM::onHTTPSessionDestroy");
        IOPerformanceCounter perf = httpSession.getIOPerformanceCounter();

        Long outbytes = perf.getMessagesOutBytes();
        getLogger().debug("WSM::Outbytes: " + outbytes);

        Double outrate = perf.getMessagesOutBytesRate();
        getLogger().debug("WSM::OutRate: " + outrate);

        Double seconds = outbytes / outrate;
        getLogger().debug("WSM::Seconds: " + seconds);

        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(httpSession));
    }

    //RTMP (Flash)
    
    public void onConnect(IClient client, RequestFunction function, AMFDataList params) {
        getLogger().debug("WSM::onConnect");
        ConnectionFactory.getConnectionInfo(client);
    }

    
    public void onConnectAccept(IClient client) {
        getLogger().debug("WSM::onConnectAccept");
        ConnectionFactory.getConnectionInfo(client);
    }

    
    public void onConnectReject(IClient client) {
        getLogger().debug("WSM::onConnectReject");
        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(client));
    }

    
    public void onDisconnect(IClient client) {
        getLogger().debug("WSM::onDisconnect");
        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(client));
    }

    
    public void onStreamCreate(IMediaStream stream) {
        getLogger().debug("WSM::onStreamCreate");

        IConnectionInfo connection = ConnectionFactory.getConnectionInfoFromStream(stream);

        if(connection != null ) {
            connection.onStreamCreate(stream);
        }
    }

    
    public void onStreamDestroy(IMediaStream stream) {
        getLogger().debug("WSM::onStreamDestroy");

        IConnectionInfo connection = ConnectionFactory.getConnectionInfoFromStream(stream);

        if(connection != null ) {
            connection.onStreamDestroy(stream);
        }
    }

//    //Cupertino (HLS, iOS)
//
//    public void onHTTPCupertinoStreamingSessionCreate(HTTPStreamerSessionCupertino httpSession) {
//        getLogger().info("WSM::onHTTPCupertinoStreamingSessionCreate");
//        ConnectionFactory.getConnectionInfo(httpSession);
//    }
//
//
//    public void onHTTPCupertinoStreamingSessionDestroy(HTTPStreamerSessionCupertino httpSession) {
//        getLogger().info("WSM::onHTTPCupertinoStreamingSessionDestroy");
//        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(httpSession));
//    }
//
//    //Smooth (MPEG-DASH, Silverlight)
//
//    public void onHTTPSmoothStreamingSessionCreate(HTTPStreamerSessionSmoothStreamer httpSession) {
//        getLogger().info("WSM::onHTTPSmoothStreamingSessionCreate");
//        ConnectionFactory.getConnectionInfo(httpSession);
//    }
//
//
//    public void onHTTPSmoothStreamingSessionDestroy(HTTPStreamerSessionSmoothStreamer httpSession) {
//        getLogger().info("WSM::onHTTPSmoothStreamingSessionDestroy");
//        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(httpSession));
//    }
//
//    //Sanjose (HDS, Flash HTTP))
//
//    public void onSanJoseStreamingSesssionCreate(HTTPStreamerSessionSanJose httpSession) {
//        getLogger().info("WSM::onSanJoseStreamingSesssionCreate");
//        ConnectionFactory.getConnectionInfo(httpSession);
//    }
//
//
//    public void onSanJoseStreamingSesssionDestroy(HTTPStreamerSessionSanJose httpSession) {
//        getLogger().info("WSM::onSanJoseStreamingSesssionDestroy");
//        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(httpSession));
//    }
//
//    //RTP (RTP, RTSP)
//
//    public void onRTPSessionCreate(RTPSession rtpSession) {
//        getLogger().info("WSM::onRTPSessionCreate");
//        ConnectionFactory.getConnectionInfo(rtpSession);
//    }
//
//
//    public void onRTPSessionDestroy(RTPSession rtpSession) {
//        getLogger().info("WSM::onRTPSessionDestroy");
//        ConnectionFactory.closeConnectionInfo(ConnectionFactory.getConnectionInfo(rtpSession));
//    }
//
}