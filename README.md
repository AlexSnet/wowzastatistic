## Installation

Put JAR file to Wowza's lib folder.
Add to EACH Application.xml file strings to load module.

Class: com.cdnvideo.wms.plugin.stats.StatsModuleMain

Example:

```
<Module>
 <Name>WowzaStatistics</Name>
 <Description>Logs usage statistic in configured intervals</Description>
 <Class>com.cdnvideo.wms.plugin.stats.StatsModuleMain</Class>
</Module>
```

## Configuration

Configuration properties in Server.xml

* **LogFeedInterval**
     Interval between feeds in milliseconds (default: 10000)

* **ServerName**
     Name of the server to print

* **GeoIPDBPath**
     Path to MaxMind's GeoIP Database v1, e.g.: /var/www/geodata/GeoIPCity.dat
     If configured than print HeoIP info in log messages


Configuration properties in Application.xml

* **ClientName**
     Name of the client


All the property names are case-sensitive and optional!


Configuration of log4j (log4j.properties) to log in separate files:

```
#### WowzaStatistics ####
log4j.logger.StatisticsLogger=INFO, stdout, wowzaStatistics

log4j.appender.wowzaStatistics=org.apache.log4j.DailyRollingFileAppender
log4j.appender.wowzaStatistics.File=/cdn/log/wowza/wowza-statistics.log
log4j.appender.wowzaStatistics.layout=com.wowza.wms.logging.ECLFPatternLayout
log4j.appender.wowzaStatistics.layout.Fields=x-comment
log4j.appender.wowzaStatistics.layout.OutputHeader=true
log4j.appender.wowzaStatistics.layout.QuoteFields=false
log4j.appender.wowzaStatistics.layout.Delimeter=tab
log4j.additivity.wowzaStatistics=false
```

## Build instructions

Just run `ant jar` to build JAR.
To build jar and automatically put it in Wowza's lib folder run `ant copy`.

If something gets wrong (no rights, etc) try to add `sudo` at the beginning.